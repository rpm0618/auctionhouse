The project was created using IntelliJ IDEA 13.5 Community Edition. Any recent version of IntelliJ should be able to run this project

I created a simple server to assist in testing. Running the jar will start
the server:

Request format:
/[command]?[params]
  
The server listens on port 8080
 
Parameters are passed in the standard URL format (It's an HTTP GET request)
 
Available commands

* start
    * itemName (String)
    * reservedPrice (Float)
* bid
    * itemName (String)
    * bidder (String)
    * bid (Float)
* close
    * itemName (String)
* query
    * itemName (String)

The server returns it's information in human readable format, for ease of testing. However, in a real-world scenario, it would return JSON or something similar

If you want to run the API without a server, There is an example in the Main
class. You can un-comment and re-build to run it that way.