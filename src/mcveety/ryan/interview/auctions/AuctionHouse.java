package mcveety.ryan.interview.auctions;

import mcveety.ryan.interview.datastore.DataStore;

/**
 * The main API. The server to would call each method to respond to requests.
 * Ryan McVeety - January 6, 2015
 */
public class AuctionHouse
{

    /**
     * Where the auction data is stored
     */
    private DataStore mDataStore;

    /**
     * Create a new auction house. Any auctions stored in the data store will be retained
     *
     * @param dataStore Where to store the auction data.
     */
    public AuctionHouse(DataStore dataStore)
    {
        mDataStore = dataStore;
    }

    /**
     * Starts a new auction with the given information
     *
     * @param itemName      The name of the item being sold.
     * @param reservedPrice The minimum price the item will go for. If the auction closes below this price, it is not sold
     * @return AUCTION_ALREADY_EXISTS, if there is already an auction for that item, or SUCCESS if the auction was created
     */
    public ResultCode startAuction(String itemName, float reservedPrice)
    {
        if (mDataStore.getAuction(itemName) != null)
        {
            return ResultCode.AUCTION_ALREADY_EXISTS;
        }

        mDataStore.updateAuction(new Auction(itemName, reservedPrice));
        return ResultCode.SUCCESS;
    }

    /**
     * Submit a bid to the given auction.
     *
     * @param itemName   The name of the item to bid on
     * @param bidderName The name of the person bidding
     * @param bid        The amount the person is bidding
     * @return AUCTION_DOESNT_EXIST, AUCTION_OVER, BID_PRICE_TO_LOW if the bid is not high enough, or SUCCESS
     */
    public ResultCode submitBid(String itemName, String bidderName, float bid)
    {
        Auction auction = mDataStore.getAuction(itemName);
        if (auction == null)
        {
            return ResultCode.AUCTION_DOESNT_EXIST;
        }
        else if (auction.getStatus() != AuctionStatus.ONGOING)
        {
            return ResultCode.AUCTION_OVER;
        }
        else if (auction.getItemCurrentPrice() >= bid)
        {
            return ResultCode.BID_PRICE_TO_LOW;
        }

        auction.setItemCurrentPrice(bid);
        auction.setBidderName(bidderName);
        mDataStore.updateAuction(auction);

        return ResultCode.SUCCESS;
    }

    /**
     * End the auction for the given item. Updates the auction status to reflect if the item was sold or not
     *
     * @param itemName The name of the auction to end.
     * @return AUCTION_DOESNT_EXIST, AUCTION_OVER if the auction has already ended, or SUCCESS
     */
    public ResultCode callAuction(String itemName)
    {
        Auction auction = mDataStore.getAuction(itemName);
        if (auction == null)
        {
            return ResultCode.AUCTION_DOESNT_EXIST;
        }
        else if (auction.getStatus() != AuctionStatus.ONGOING)
        {
            return ResultCode.AUCTION_OVER;
        }
        else if (auction.getItemCurrentPrice() > auction.getItemReservedPrice())
        {
            auction.setStatus(AuctionStatus.SUCCESSFUL);
        }
        else
        {
            auction.setStatus(AuctionStatus.FAILED);
        }

        return ResultCode.SUCCESS;
    }

    /**
     * Get data on the auction with the given name.
     *
     * @param itemName The name of the item to retrieve data on
     * @return The auction representing the given name, or null if no auction exists
     */
    public Auction queryAuction(String itemName)
    {
        return mDataStore.getAuction(itemName);
    }
}
