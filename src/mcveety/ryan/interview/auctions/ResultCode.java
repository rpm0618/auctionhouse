package mcveety.ryan.interview.auctions;

/**
 * The different return values for each of the API methods
 * Ryan McVeety - January 6, 2015
 */
public enum ResultCode
{
    SUCCESS,
    AUCTION_DOESNT_EXIST,
    AUCTION_ALREADY_EXISTS,
    AUCTION_OVER,
    BID_PRICE_TO_LOW
}
