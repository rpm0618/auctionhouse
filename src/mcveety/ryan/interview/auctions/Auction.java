package mcveety.ryan.interview.auctions;

/**
 * A container object that encapsulates all of the auction data
 * Ryan McVeety - January 6, 2015
 */
public class Auction
{
    private String mItemName;
    private float mItemReservedPrice;
    private float mItemCurrentPrice;

    private AuctionStatus mStatus;
    private String mBidderName;

    /**
     * Initialize a new Auction.
     *
     * @param itemName          The name of the item being sold
     * @param itemReservedPrice The minimum price that the item must sell for
     */
    public Auction(String itemName, float itemReservedPrice)
    {
        mItemName = itemName;
        mItemReservedPrice = itemReservedPrice;
        mItemCurrentPrice = 0;

        mStatus = AuctionStatus.ONGOING;
        mBidderName = "";
    }

    /**
     * @return The name of the item being auctioned
     */
    public String getItemName()
    {
        return mItemName;
    }

    /**
     * @return The minimum price the item must go for
     */
    public float getItemReservedPrice()
    {
        return mItemReservedPrice;
    }

    /**
     * @return The current price of the item
     */
    public float getItemCurrentPrice()
    {
        return mItemCurrentPrice;
    }

    /**
     * Does no validation on price (ie, above current bid, non-negative, etc)
     *
     * @param itemCurrentPrice The new price of the item
     */
    public void setItemCurrentPrice(float itemCurrentPrice)
    {
        this.mItemCurrentPrice = itemCurrentPrice;
    }

    /**
     * @return The status of the auction
     */
    public AuctionStatus getStatus()
    {
        return mStatus;
    }

    /**
     * @param status The auction's new status
     */
    public void setStatus(AuctionStatus status)
    {
        this.mStatus = status;
    }

    /**
     * @return The name of the person with the current highest bid
     */
    public String getBidderName()
    {
        return mBidderName;
    }

    /**
     * @param bidderName The new name of the person with the highest bid
     */
    public void setBidderName(String bidderName)
    {
        mBidderName = bidderName;
    }

    @Override
    public String toString()
    {
        if (mStatus == AuctionStatus.ONGOING)
        {
            return "The auction for " + mItemName + " is ongoing. Current price is $" + mItemCurrentPrice;
        }
        else if (getStatus() == AuctionStatus.SUCCESSFUL)
        {
            return mItemName + " was sold to " + mBidderName + " for the price of $" + mItemCurrentPrice;
        }
        else
        {
            return mItemName + " failed to reach the reserved price and was not sold";
        }
    }
}
