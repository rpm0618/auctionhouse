package mcveety.ryan.interview.auctions;

/**
 * The different states an auction can be in.
 * Ryan McVeety - January 6, 2015
 */
public enum AuctionStatus
{
    ONGOING,
    SUCCESSFUL,
    FAILED
}
