package mcveety.ryan.interview;

import mcveety.ryan.interview.auctions.Auction;
import mcveety.ryan.interview.auctions.AuctionHouse;
import mcveety.ryan.interview.datastore.HashMapDataStore;

import java.io.IOException;

/**
 * A simple testbed for the auction house API. This project was written in response to one of the pre-interview
 * questions for the Hackerati interview.
 *
 * Runs an extremely simple web server that accepts commands. See BasicServer for more info on syntax
 * Ryan McVeety - January 6, 2015
 */
public class Main
{
    public static void main(String[] args) throws IOException
    {
        // Uncomment this section for an example of calling the API directly

//        AuctionHouse house = new AuctionHouse(new HashMapDataStore());
//        house.startAuction("Blue Car", 100);
//        house.submitBid("Blue Car", "Ryan", 50);
//        house.submitBid("Blue Car", "Megan", 120);
//        house.callAuction("Blue Car");
//        Auction auction = house.queryAuction("Blue Car");
//        if (auction != null)
//        {
//            System.out.println(auction);
//        }
//        else
//        {
//            System.out.println("Auction doesn't exist");
//        }

        //Create and start the server
        BasicServer server = new BasicServer();
        server.start();
        server.run();
    }
}
