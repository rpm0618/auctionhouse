package mcveety.ryan.interview;

import mcveety.ryan.interview.auctions.Auction;
import mcveety.ryan.interview.auctions.AuctionHouse;
import mcveety.ryan.interview.auctions.ResultCode;
import mcveety.ryan.interview.datastore.HashMapDataStore;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.HashMap;

/**
 * A very simple web server, basically just forwards calls straight through to the AuctionHouse API. Request format:
 * /[command]?[params]
 *
 * The server listens on port 8080
 *
 * Parameters are passed in the standard URL format (It's an HTTP GET request)
 *
 * Available commands:
 * - start
 *      itemName (String)
 *      reservedPrice (Float)
 * - bid
 *      itemName (String)
 *      bidder (String)
 *      bid (Float)
 * - close
 *      itemName (String)
 * - query
 *      itemName (String)
 *
 * The server returns it's information in human readable format, for ease of testing. However, in a real-world scenario,
 * It would return JSON or something similar
 * Ryan McVeety - January 6, 2015
 */
public class BasicServer
{
    private ServerSocket mServer;

    private AuctionHouse mHouse;

    public BasicServer()
    {
        mHouse = new AuctionHouse(new HashMapDataStore());
    }

    /**
     * Start the server. All it does is open up the port 8080 for listening
     *
     * @throws IOException
     */
    public void start() throws IOException
    {
        mServer = new ServerSocket(8080);
    }

    /**
     * Start handling requests. THIS IS A BLOCKING CALL. It will loop until the program is shutdown or an exception is
     * thrown
     *
     * @throws IOException
     */
    public void run() throws IOException
    {
        while (true)
        {
            Socket conn = mServer.accept();
            try
            {
                handleRequest(conn);
            }
            catch (RuntimeException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Parse the parameters from a url
     *
     * @param url The full url to extract the parameters from
     * @return A map with parameters names as keys
     */
    private HashMap<String, String> getParams(String url)
    {
        HashMap<String, String> params = new HashMap<String, String>();
        //URL parameters are in the format abc?one=1&two=2
        //This line extracts each "pair" (name and value) from the url
        String[] pairs = url.split("\\?")[1].split("&");
        for (String pair : pairs)
        {
            //The first token will be the parameter name, the second it's value
            String[] tokens = pair.split("=");
            try
            {
                //Puts each pair into the map, making sure that it's properly decoded
                params.put(URLDecoder.decode(tokens[0], "UTF-8"), URLDecoder.decode(tokens[1], "UTF-8"));
            }
            //If UTF-8 isn't supported, then there are bigger issues than URL decoding
            catch (UnsupportedEncodingException e)
            {
            }
        }
        return params;
    }

    /**
     * Get the command this url is trying to execute. See the class JavaDoc for more info on format
     *
     * @param url The url to extract the command from
     * @return The command stated in the url
     */
    private String getCommand(String url)
    {
        return url.split(" ")[1].split("\\?")[0].substring(1);
    }

    private void handleRequest(Socket conn)
    {
        try
        {
            //Reading and writing directly to streams can be tedious
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            PrintWriter out = new PrintWriter(conn.getOutputStream());

            //The first line of every HTTP request is the request string.
            String request = in.readLine();

            String command = getCommand(request);

            String response = "";

            if (command.equals("start"))
            {
                response = handleStart(request);
            }
            else if (command.equals("bid"))
            {
                response = handleBid(request);
            }
            else if (command.equals("close"))
            {
                response = handleClose(request);
            }
            else if (command.equals("query"))
            {
                response = handleQuery(request);
            }
            else
            {
                response = "Invalid Command";
            }

            //Send out an HTTP Response
            out.println("HTTP/1.0 200");
            out.println("Content-type: text/html");
            out.println("Content-length: " + response.toCharArray().length);
            out.println();
            out.println(response);
            out.flush();
            out.close();
            conn.close();
        }
        catch (IOException e)
        {
            System.out.println("ERROR: " + e.getMessage());
        }
        finally
        {
            //Try and make sure that the connection is closed
            if (conn != null)
            {
                try
                {
                    conn.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Starts an auction
     *
     * @param request The entire request string
     * @return A human readable output
     */
    private String handleStart(String request)
    {
        //Parse the url parameters
        String url = request.split(" ")[1];
        HashMap<String, String> params = getParams(url);
        String itemName = params.get("itemName");
        float reservedPrice = Float.parseFloat(params.get("reservedPrice"));

        //Basic logging
        System.out.println("Start command received: " + itemName + " $" + reservedPrice);

        //API call
        ResultCode res = mHouse.startAuction(itemName, reservedPrice);

        if (res == ResultCode.SUCCESS)
        {
            return "Auction for " + itemName + " with a reserved price of $" + reservedPrice + " started";
        }
        else
        {
            return "An auction for " + itemName + " already exists";
        }
    }

    /**
     * Submits a bid
     *
     * @param request The entire request string
     * @return A human readable output
     */
    private String handleBid(String request)
    {
        //Parse URL parameters
        String url = request.split(" ")[1];
        HashMap<String, String> params = getParams(url);
        String itemName = params.get("itemName");
        String bidder = params.get("bidder");
        float bid = Float.parseFloat(params.get("bid"));

        //Basic logging
        System.out.println("Bid command received: " + bidder + " " + itemName + " $" + bid);

        //API call
        ResultCode res = mHouse.submitBid(itemName, bidder, bid);

        if (res == ResultCode.SUCCESS)
        {
            return "Bid submitted for " + itemName + " by " + bidder + " at a price of $" + bid;
        }
        else if (res == ResultCode.BID_PRICE_TO_LOW)
        {
            return "Submitted bid of $" + bid + " is to low";
        }
        else
        {
            return "No auction for " + itemName + " exists";
        }
    }

    /**
     * Closes an auction
     *
     * @param request The entire request string
     * @return A human readable output
     */
    private String handleClose(String request)
    {
        //Parse URL parameters
        String url = request.split(" ")[1];
        HashMap<String, String> params = getParams(url);
        String itemName = params.get("itemName");

        //Basic logging
        System.out.println("Close command received: " + itemName);

        //API call
        ResultCode res = mHouse.callAuction(itemName);

        if (res == ResultCode.SUCCESS)
        {
            return "Auction for " + itemName + " has been closed";
        }
        else
        {
            return "No auction for " + itemName + " exists";
        }
    }

    /**
     * Queries the auction house
     *
     * @param request The entire request string
     * @return A human readable output
     */
    private String handleQuery(String request)
    {
        //Parse URL parameters
        String url = request.split(" ")[1];
        HashMap<String, String> params = getParams(url);
        String itemName = params.get("itemName");

        //Basic logging
        System.out.println("Query command received: " + itemName);

        //API call
        Auction auction = mHouse.queryAuction(itemName);

        if (auction != null)
        {
            return auction.toString();
        }
        else
        {
            return "No auction for " + itemName + " exists";
        }
    }
}
