package mcveety.ryan.interview.datastore;

import mcveety.ryan.interview.auctions.Auction;

/**
 * Abstracts away the code needed to retrieve and store data, so different methods can be interchanged easily
 * Ryan McVeety - January 6, 2015
 */
public interface DataStore
{
    /**
     * Create the auction if it doesn't exist, or update it's data if it does
     *
     * @param auction The data to update
     */
    void updateAuction(Auction auction);

    /**
     * Get the auction data for the given name
     *
     * @param itemName The name of the auction to retrieve
     * @return The requested auction, or null if none with that name exists
     */
    Auction getAuction(String itemName);
}
