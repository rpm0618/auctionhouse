package mcveety.ryan.interview.datastore;

import mcveety.ryan.interview.auctions.Auction;

import java.util.HashMap;

/**
 * A simple data store for testing purposes, uses a HashMap behind the scenes
 * Ryan McVeety - January 6, 2015
 */
public class HashMapDataStore implements DataStore
{
    HashMap<String, Auction> mAuctions;

    public HashMapDataStore()
    {
        mAuctions = new HashMap<String, Auction>();
    }

    @Override
    public void updateAuction(Auction auction)
    {
        mAuctions.put(auction.getItemName(), auction);
    }

    @Override
    public Auction getAuction(String itemName)
    {
        return mAuctions.get(itemName);
    }
}
